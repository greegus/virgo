<?php get_header(); ?>

<section id="category">
	<div class="wrapper">
		<div class="columns">
			<main class="column column-8">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Category Archives: ', 'blankslate' ); ?><?php single_cat_title(); ?></h1>
					<?php if ( '' != category_description() ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . category_description() . '</div>' ); ?>
				</header>

				<div class="category-posts">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'entry' ); ?>
					<?php endwhile; endif; ?>
				</div>

				<?php get_template_part( 'nav', 'below-feed' ); ?>
			</main>

			<div class="column column-4">
				get_sidebar();
			</div>
	
		</div>
	</div>
</section>

<?php get_footer(); ?>