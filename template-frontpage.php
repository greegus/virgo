<?php 

/**
 * Template Name: Frontpage
 */

get_header();

?>

<section id="front-page">
	<div class="columns">
		<main class="column column-12">
			<article>
				<h1><?php bloginfo('title') ?></h1>
				<p><?php bloginfo('description') ?></p>
			</article>
		</main>
	</div>
</section>

<?php

get_footer();

?>