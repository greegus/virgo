<form id="searchform" action="<?php bloginfo("url") ?>" method="get">
	<input type="text" name="s" value="<?php the_search_query(); ?>" placeholder="<?php _e("Search in", "virgo") ?>" />
	<button type="button"><?php _e("Search!", "virgo") ?></button>
</form>