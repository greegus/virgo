<?php get_header(); ?>

<section id="archive">
	<div class="wrapper">
		<div class="columns">
			<main class="column column-8">
				<header class="page-header">
					<h1 class="page-title">
						<?php if ( is_day() ) { printf( __( 'Daily Archives: %s', 'blankslate' ), get_the_time( get_option( 'date_format' ) ) ); } 
							elseif ( is_month() ) { printf( __( 'Monthly Archives: %s', 'blankslate' ), get_the_time( 'F Y' ) ); } 
							elseif ( is_year() ) { printf( __( 'Yearly Archives: %s', 'blankslate' ), get_the_time( 'Y' ) ); } 
							else { _e( 'Archives', 'blankslate' ); }
						?>
					</h1>
				</header>

				<div class="archive-posts">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'entry' ); ?>
					<?php endwhile; endif; ?>
				</div>
				
				<?php get_template_part( 'nav', 'below-feed' ); ?>
				
			</main>
		</div>

		<div class="column column-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>