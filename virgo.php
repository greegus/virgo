<?php 

add_action( "after_setup_theme", "virgo_setup" );

function virgo_setup() {
	load_theme_textdomain( "virgo", get_template_directory() . "/languages" );

	add_theme_support("automatic-feed-links");
	add_theme_support("post-thumbnails");
	add_theme_support("post-formats");
	add_theme_support("html5");
	
	register_nav_menus(array(
		"main-menu" => __( "Main Menu", "virgo" ),
		"footer-menu" => __( "Footer Menu", "virgo" )
	));

	register_sidebar(array(
		"name" => __("Sidebar", "virgo"),
		"id" => "sidebar",
		"before_title"  => "<h3>",
		"after_title"   => "</h3>"
	));

	add_image_size("virgo-thumbnail", 300, 300, true);
}



add_action("wp_enqueue_scripts", "blankslate_load_scripts");

function blankslate_load_scripts() {
	wp_enqueue_script("jquery");
	
	wp_enqueue_script("angular", "//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.13/angular.min.js" );
	wp_enqueue_script("angular-i18n-en-us", "//cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.13/i18n/angular-locale_cs.js" );
	
	wp_enqueue_script("scripts", get_template_directory_uri() . "/scripts/scripts.js");	

	wp_enqueue_style("font-awesome", "//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css");
}



add_filter("excerpt_length", "virgo_excerpt_length", 999);

function virgo_excerpt_length($length) {
	return 120;
}



add_filter("excerpt_more", "virgo_excerpt_more");

function virgo_excerpt_more($more) {
	return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">' . __('Read More', 'virgo') . '</a>';
}



add_action("admin_bar_init", "virgo_admin_bar_init");

function virgo_admin_bar_init() {
    remove_action("wp_head", "_admin_bar_bump_cb");
}


add_filter("media_view_settings", "virgo_gallery_default_type_set_link");

function virgo_gallery_default_type_set_link($settings) {
    $settings["galleryDefaults"]["link"] = "file";
    $settings["galleryDefaults"]["columns"] = "5";
    $settings["galleryDefaults"]["size"] = "virgo-thumbnail";
    
    return $settings;
}

add_filter('image_size_names_choose', 'virgo_image_sizes');
	
function virgo_image_sizes($sizes) {
	$newsizes = array_merge($sizes, array(
		"virgo-thumbnail" => __( "Larger thumbnail (Virgo)")
	));
	
	return $newsizes;
}



/* 
********************************************************************************************************/

function virgo_the_perex($length = 120, $more = '&hellip;') {
	echo virgo_get_the_perex(get_the_ID(), $length, $more);
}


function virgo_get_the_perex($post_id, $length = 120, $more = '&hellip;') {

	$content = get_the_content($post_id);
	$content = preg_replace("/" . get_shortcode_regex() . "/s", "", $content);
	$content = strip_tags(apply_filters("the_content", $content));

	preg_match('/\<\!\-\-\s*more\s*\-\-\>/', $content, $moreTagPosition, PREG_OFFSET_CAPTURE);

	if ($moreTagPosition) {
		$content = strip_tags(substr($content, 0, $moreTagPosition[0][1]));

	} else {
		$content = strip_tags($content);

		if (strlen($content) > $length) {
			$content = substr($content, 0, $length + 1);
			$lastSpacePosition = strrpos($content, ' ');

			if ($lastSpacePosition)
				$content = substr($content, 0, $lastSpacePosition - 1) . ($more ? $more : '');
		}
	}

	return $content;
}


function virgo_get_url($suburl = "") {
	return esc_url(home_url() . ($suburl ? "/" . ltrim($suburl, "/") : ""));
}


function virgo_get_theme_url($suburl = "") {
	return esc_url(get_template_directory_uri() . ($suburl ? "/" . ltrim($suburl, "/") : ""));
}

function virgo_get_uploads_url($suburl = "") {
	return esc_url(wp_upload_dir()["baseurl"] . ($suburl ? "/" . ltrim($suburl, "/") : ""));
}


function virgo_get_thumbnail($post_id = null, $size = null) {
	return wp_get_attachment_image_src(get_post_thumbnail_id($post_id ?: get_the_ID()), $size ?: "virgo-thumbnail")[0];
}


function virgo_thumbnail_as_img($post_id = null, $size = null) {
	echo '<img src="' . virgo_get_thumbnail($post_id ?: get_the_ID(), $size) . '" alt="' . get_the_title($post_id ?: get_the_ID()) . '" />';
}


function virgo_thumbnail_as_bg($post_id = null, $size = null) {
	echo 'background: url(\'' . virgo_get_thumbnail($post_id ?: get_the_ID(), $size) . '\') no-repeat center / cover';
}


function virgo_get_the_meta($key, $post_id = null) {
	return get_post_meta($post_id ?: get_the_ID(), $key, true);
}


function virgo_get_post_gallery_images($post_id = null, $size = null, $all_galleries = false) {
	preg_match_all('/'. get_shortcode_regex() .'/s', get_the_content($post_id), $matches);

	$thumbnails_urls = array();

	foreach ($matches[2] as $index => $shortcode) {
		if ($shortcode == "gallery") {
			$attributes = shortcode_parse_atts($matches[3][$index]);
			$attachements_ids = explode(",", $attributes["ids"]);

			foreach ($attachements_ids as $attachement_id)
				$thumbnails_urls[] = wp_get_attachment_image_src($attachement_id, $size ?: "virgo-thumbnail")[0];

			if (!$all_galleries)
				break;
		}
	}

	return $thumbnails_urls;
}


function virgo_get_post_thumbnail_url($post_id = null) {
	return wp_get_attachment_url(get_post_thumbnail_id($post_id ?: get_the_ID()));
}


function virgo_get_post_video_thumbnail($post_id = null, $size = "hqdefault") {
	preg_match('/(youtu\.be\/|youtube\.com\/(watch\?(.*&)?v=|(embed|v)\/))([^\?&"\'>]+)/s', get_the_content($post_id), $match);

	$thumbnail_url = null;

	if ($match) {
		// available sizes = default, hqdefault, mqdefault, sddefault, maxresdefault
		$thumbnail_url = 'http://img.youtube.com/vi/' . $match[5] . '/' . $size . '.jpg';
	}

	return $thumbnail_url;

}


function virgo_pagination_links() {
	global $wp_query;

	$big = 999999999; 

	echo paginate_links(array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format' => '?paged=%#%',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $wp_query->max_num_pages
	));
}


function virgo_include($template, $data = array(), $echo = true) {
	$__echo = $echo;

	ob_start();

	extract($data);
	
	include(locate_template($template . '.php'));
	
	$__content = ob_get_clean();
	
	if ($__echo)
		echo $__content;
	else
		return $__content;
}


function virgo_load_theme_includes() {
	static $loaded = false;

	if ($loaded)
		return;

	$loaded = true;
	$includesDir = get_template_directory() . "/includes";

	if (is_dir($includesDir)) {
		foreach (scandir($includesDir, SCANDIR_SORT_NONE) as $file) {
			if (preg_match("/\.php$/", $file) && !is_dir($filePath = $includesDir . "/" . $file))
				require_once ($filePath);
		}
	}
}
