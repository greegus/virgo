<!DOCTYPE html>
<html <?php language_attributes(); ?> ng-app="app">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	
	<title><?php wp_title( ' | ', true, 'right' ); ?><?php bloginfo( 'name' ) ?></title>

	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	
	<header id="header" role="banner">
		<div class="wrapper">
			<?php if ( is_front_page() ) : ?><h1 id="logo"><?php else : ?><a id="logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php esc_attr_e( get_bloginfo( 'name' ), 'virgo' ); ?>" rel="home"><?php endif ?>
			<strong><?php echo esc_html( get_bloginfo( 'name' ) ); ?></strong>
			<?php if ( is_front_page() ) : ?></h1><?php else : ?></a><?php endif ?>

			<p class="site-description">
				<?php bloginfo( 'description' ); ?>
			</p>

			<nav id="navigation" role="navigation">
				<?php 
					wp_nav_menu(array( 
						"menu" => "main-menu",
						"depth" => 0
					)); 
				?>
			</nav>
			
			<aside id="search">
				<?php get_search_form(); ?>
			</aside>
		</div>
	</header>

	<div id="content">
