<?php get_header(); ?>

<section id="page">
	<div class="wrapper">
		<div class="columns">
			<main class="column column-8">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'entry' ); ?>
				<?php endwhile; endif; ?>
			</main>

			<div class="column column-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>