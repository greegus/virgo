<?php get_header(); ?>

<section id="author">
	<div class="wrapper">
		<div class="columns">
			<main class="column column-8">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Author Archives', 'blankslate' ); ?>: <?php the_author_link(); ?></h1>
				</header>

				<?php the_post(); ?>

				<section class="author">
					<div class="author-bio">
						<?php echo get_the_author_meta( 'user_description' ) ?>
					</div>
				</section>

				<?php rewind_posts(); ?>
				
				<section class="author-posts">
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'entry' ); ?>
					<?php endwhile; ?>
				</section>

				<?php get_template_part( 'nav', 'below-feed' ); ?>
			</main>

			<div class="column column-4">
				<?php the_sidebar() ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>