<?php get_header(); ?>

<section id="tag">
	<div class="wrapper">
		<div class="columns">
			<main class="column column-8">
				<header class="page-header">
					<h1 class="page-title">
						<?php _e( 'Tag Archives: ', 'virgo' ); ?><?php single_tag_title(); ?>
					</h1>
				</header>

				<div class="taged-posts">
					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'entry' ); ?>
					<?php endwhile; endif; ?>
				</div>
				
				<?php get_template_part( 'nav', 'below' ); ?>
			</main>
			
			<div class="column column-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>