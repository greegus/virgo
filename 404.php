<?php get_header(); ?>

<section id="not-found">
	<div class="wrapper">
		<div class="columns">
			<main class="column column-8">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Not Found', 'virgo' ); ?></h1>
				</header>
				
				<div class="page-content">
					<p><?php _e( 'Nothing found for the requested page. Try a search instead?', 'virgo' ); ?></p>
				</div>
			</main>
		</div>

		<div class="column column-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>