<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) : ?>
<nav class="feed-navigation" role="navigation">
	<div class="previous"><?php next_posts_link(sprintf( __( '%s older', 'virgo' ), '<i class="fa fa-angle-left"></i>' ) ) ?></div>
	<div class="next"><?php previous_posts_link(sprintf( __( 'newer %s', 'virgo' ), '<i class="fa fa-angle-right"></i>' ) ) ?></div>
</nav>
<?php endif; ?>