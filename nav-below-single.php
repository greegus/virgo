<nav id="entry-navigation" role="navigation">
	<div class="previous"><?php previous_post_link( '%link', '<i class="fa fa-angle-left"></i> %title' ); ?></div>
	<div class="next"><?php next_post_link( '%link', '%title <i class="fa fa-angle-right"></i>' ); ?></div>
</nav>