<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if (is_singular()) : ?>
		<h1 class="entry-title">
			<?php the_title(); ?>
		</h1>
		<?php else : ?>
		<h2 class="entry-title">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
				<?php the_title(); ?>
			</a>
		</h2>
		<?php endif ?>
		
		<div class="entry-meta">
			<span class="entry-author">
				<?php the_author_posts_link(); ?>
			</span>
			
			<time datetime="<?php echo the_time('c') ?>" class="entry-date">
				<?php the_time( get_option( 'date_format' ) ); ?>
			</time>
		</div>
		
		<?php $thumbnail = virgo_get_thumbnail(get_the_ID()) ?>

		<div class="entry-thumbnail" <?php echo $var ?>>
			<?php if ($thumbnail) : ?>
			<img src="<?php echo $thumbnail ?>" alt="">
			<?php endif ?>
		</div>
	</header>

	<?php if (is_archive() || is_search() || is_front_page() || is_home()) : ?>
	<div class="entry-summary">
		<?php virgo_the_perex() ?>
	</div>
	<?php else : ?>
	<div class="entry-content">
		<?php the_content() ?>
	</div>
	<?php endif ?>

	<?php if (!post_password_required()) : ?>
	<aside class="entry-comments">
		<?php comments_template(null, true); ?>
	</aside>
	<?php endif ?>
	
	<footer class="entry-footer">
		<?php edit_post_link(); ?>
	</footer>
</article>