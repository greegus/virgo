<aside id="sidebar" role="complementary">
	<?php if ( is_active_sidebar( 'sidebar' ) ) : ?>
		<div class="widget-area">
			<ul>
				<?php dynamic_sidebar( 'sidebar' ); ?>
			</ul>
		</div>
	<?php endif; ?>
</aside>