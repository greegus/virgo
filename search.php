<?php get_header(); ?>

<section id="search-results">
	<div class="wrapper">
		<div class="columns">
			<main class="column column-8">
				<header class="page-header">
					<h1 class="page-title">
						<?php printf( __( 'Search Results for: %s', 'virgo' ), get_search_query() ); ?>
					</h1>
				</header>

				<div class="search-results">
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'entry' ); ?>
						<?php endwhile; ?>
					<?php else : ?>
						<article class="post no-results not-found">
							<header class="entry-header">
								<h2 class="entry-title">
									<?php _e( 'Nothing Found', 'virgo' ); ?>
								</h2>
							</header>

							<section class="entry-content">
								<p><?php _e( 'Sorry, nothing matched your search. Please try again.', 'virgo' ); ?></p>
								<?php get_search_form(); ?>
							</section>
						</article>
					<?php endif; ?>
				</div>

				<?php get_template_part( 'nav', 'below' ); ?>
			</main>

			<div class="column column-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>