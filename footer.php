</div> <!-- /content -->

<footer id="footer" role="contentinfo">
	<div id="copyright">
		<?php echo sprintf( __( '%1$s %2$s %3$s. All Rights Reserved.', 'virgo' ), '&copy;', date( 'Y' ), esc_html( get_bloginfo( 'name' ) ) ); ?>
	</div>
</footer>

</body>
</html>