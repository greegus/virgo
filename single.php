<?php get_header(); ?>

<section id="single" role="main">
	<div class="columns">
		<main class="column column-8">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'entry' ); ?>
			<?php endwhile; endif; ?>
			
			<footer class="footer">
				<?php get_template_part( 'nav', 'below-single' ); ?>
			</footer>
		</main>

		<div class="column column-4">
			<?php get_sidebar(); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>